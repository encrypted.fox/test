## Frotend dependencies & starting frontend

```bash
cd frontend 

npm install

npm start
```

## Starting backend

```bash
cd ../

cd backend

python manage.py runserver 8000
```

## API

Go to the http://127.0.0.1:8000/api/v0/.
You can change or view database.

## Frontend

Go to http://localhost:3000.
Click send to confirm all inputs.