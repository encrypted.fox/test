from rest_framework import serializers
from .models import Option


class OptionSerializer(serializers.ModelSerializer):
    class Meta:
        fields = (
            'main',
            'main2',
            'main3',
            'main4',
            'name',
            'name2',
            'name3',
            'name4',
            'name5',
            'name6',
            'name7',
            'name8',
            'name9',
            'name9',
            'name10',
            'name11',
            'name12',
            'name13',
            'name14',
            'name15'
        )
        model = Option
