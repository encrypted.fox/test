# todos/urls.py
from django.urls import path

from . import views

urlpatterns = [
    path('', views.ListOption.as_view()),
    path('<int:pk>/', views.DetailOption.as_view()),
    path('create/', views.create)
]