from django.shortcuts import render, HttpResponse

from rest_framework import generics
from django.views.decorators.csrf import csrf_exempt

from .models import Option
from .serializers import OptionSerializer


class ListOption(generics.ListCreateAPIView):
    queryset = Option.objects.all()
    serializer_class = OptionSerializer


class DetailOption(generics.RetrieveUpdateDestroyAPIView):
    queryset = Option.objects.all()
    serializer_class = OptionSerializer


@csrf_exempt
def create(request):
    if request.method == "POST":
        req = eval(request.body)
        print(req)
        answer = Option()
        answer.name = float(req['name'])
        answer.name2 = float(req['name2'])
        answer.name3 = float(req['name3'])
        answer.name4 = float(req['name4'])
        answer.name5 = float(req['name5'])
        answer.name6 = float(req['name6'])
        answer.name7 = float(req['name7'])
        answer.name8 = float(req['name8'])
        answer.name9 = float(req['name9'])
        answer.name10 = float(req['name10'])
        answer.name11 = float(req['name11'])
        answer.name12 = float(req['name12'])
        answer.name13 = float(req['name13'])
        answer.name14 = float(req['name14'])
        answer.name15 = float(req['name15'])
        answer.main = float(req['main'])
        answer.main2 = float(req['main2'])
        answer.main3 = float(req['main3'])
        answer.main4 = float(req['main4'])
        answer.save()
    return HttpResponse(200)
