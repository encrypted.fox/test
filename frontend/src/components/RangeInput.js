import React, {Component} from 'react';

class RangeInput extends Component {

    constructor(props) {
        super(props);
        this.state = {value: null, printedValue: null};

        this.handleChange = this.handleChange.bind(this);
        this.handleValue = this.handleValue.bind(this);
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    };

    handleValue() {
        let prevVal = parseFloat(this.state.printedValue || this.props.initialValue || 0);
        this.setState({printedValue: this.state.value});
        let curVal = parseFloat(this.state.value || 0);
        let diff = curVal - prevVal;
        this.props.setSummaryValue(diff);
    };

    render() {
        return <div className="flex-cell">
            <div className="flex-string">
                <label htmlFor={this.props.name}>{this.props.label}</label>
                <span>{this.props.description}</span>
                <span>{this.state.printedValue || this.props.initialValue}%</span>
            </div>
            <input type="range"
                   name={this.props.name}
                   value={this.state.value || this.props.initialValue}
                   min="0"
                   max="100"
                   onChange={this.handleChange}
                   onMouseUp={this.handleValue}
                   onTouchEnd={this.handleValue}
                   step="1"/>
        </div>
    }
}

export default RangeInput;