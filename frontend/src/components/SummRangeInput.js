import React, {Component} from 'react';

class SummRangeInput extends Component {
    render() {
        return <div className="flex-cell">
            <div className="flex-string">
                <label htmlFor={this.props.name}>{this.props.label}</label>
                <span>{this.props.description}</span>
                <span>{this.props.summary || this.props.initialValue}%</span>
            </div>
            <input type="range"
                   name={this.props.name}
                   value={this.props.summary || this.props.initialValue}
                   min="0"
                   max="100"
                   step="0.01"
                   readOnly/>
        </div>
    }
}

export default SummRangeInput;