import React from 'react';
import './App.css';
import RangeInputBlock from "./RangeInputBlock";
import axios from 'axios'

function App() {

    let data = [
        {
            name: "name",
            label: "label",
            description: "desc",
            initialValue: "10"
        },
        {
            name: "name1",
            label: "label1",
            description: "desc1",
            initialValue: "25"
        },
        {
            name: "name2",
            label: "label2",
            description: "desc1",
            initialValue: "35"
        },
        {
            name: "name3",
            label: "label3",
            description: "desc1",
            initialValue: "5"
        }
    ];

    let data2 = [
        {
            name: "name4",
            label: "label",
            description: "desc",
            initialValue: "10"
        },
        {
            name: "name5",
            label: "label1",
            description: "desc1",
            initialValue: "25"
        },
        {
            name: "name6",
            label: "label2",
            description: "desc1",
            initialValue: "35"
        },
        {
            name: "name7",
            label: "label3",
            description: "desc1",
            initialValue: "5"
        }
    ];

    let data3 = [
        {
            name: "name8",
            label: "label",
            description: "desc",
            initialValue: "10"
        },
        {
            name: "name9",
            label: "label1",
            description: "desc1",
            initialValue: "25"
        },
        {
            name: "name10",
            label: "label2",
            description: "desc1",
            initialValue: "35"
        },
        {
            name: "name11",
            label: "label3",
            description: "desc1",
            initialValue: "5"
        }
    ];

    let data4 = [
        {
            name: "name12",
            label: "label",
            description: "desc",
            initialValue: "10"
        },
        {
            name: "name13",
            label: "label1",
            description: "desc1",
            initialValue: "25"
        },
        {
            name: "name14",
            label: "label2",
            description: "desc1",
            initialValue: "35"
        },
        {
            name: "name15",
            label: "label3",
            description: "desc1",
            initialValue: "5"
        }
    ];

    function findInitialSum(data) {
        let summ = 0;
        for (let i = 0; i < data.length; i++) {
            summ += parseFloat(data[i].initialValue);
        }
        return summ / data.length;
    }

    function handleSubmit(event) {
        event.preventDefault();
        let body = {};
        document.querySelectorAll("#form input[type='range']").forEach(el => body[el.name] = el.value);
        console.log(body);
        axios({
            method: 'post',
            url: 'http://127.0.0.1:8000/api/v0/create/',
            data: body
        });
    }

    return (
        <div className="container">
            <form action="http://127.0.0.1:8000/api/v0/create/" method="POST" id="form">
                <RangeInputBlock
                    name="main"
                    label="CPU"
                    description="description"
                    initialValue={findInitialSum(data)}
                    categoryName="category"
                    data={data}/>
                <RangeInputBlock
                    name="main2"
                    label="CPU"
                    description="description"
                    initialValue={findInitialSum(data2)}
                    categoryName="category"
                    data={data2}/>
                <RangeInputBlock
                    name="main3"
                    label="CPU"
                    description="description"
                    initialValue={findInitialSum(data3)}
                    categoryName="category"
                    data={data3}/>
                <RangeInputBlock
                    name="main4"
                    label="CPU"
                    description="description"
                    initialValue={findInitialSum(data4)}
                    categoryName="category"
                    data={data4}/>
                <input type="submit" value="Send" onClick={handleSubmit}/>
            </form>
        </div>
    );
}

export default App;
