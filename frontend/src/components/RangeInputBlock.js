import React, {Component} from 'react';
import RangeInput from "./RangeInput";
import SummRangeInput from "./SummRangeInput";

class RangeInputBlock extends Component {

    constructor(props) {
        super(props);
        this.state = {summary: this.props.initialValue || 0, value: null};

        this.setSummaryValue = this.setSummaryValue.bind(this);
    }

    setSummaryValue(value) {
        this.setState({summary: this.state.summary + value / this.props.data.length});
    }

    render() {
        return <div className="block">
            <h2>{this.props.categoryName}</h2>

            <SummRangeInput name={this.props.name}
                            label={this.props.label}
                            description={this.props.description}
                            summary={this.state.summary}
                            initialValue={this.props.initialValue}/>

            {this.props.data.map((el) =>
                <RangeInput name={el.name}
                            label={el.label}
                            initialValue={el.initialValue}
                            description={el.description}
                            key={el.name}
                            setSummaryValue={this.setSummaryValue}/>)}
        </div>
    }
}

export default RangeInputBlock;